package org.example.email.service;

import org.example.email.api.MailSenderService;

/**
 * @author lihuazeng
 */
public class MailSernderServiceImpl implements MailSenderService {
    @Override
    public boolean send(String email, String emailEntry) {
        System.out.println("发送邮件到邮箱："+email+"。内容："+emailEntry);
        return true;
    }
}
