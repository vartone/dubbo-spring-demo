package org.example.email.service;

import org.apache.dubbo.container.Main;

/**
 * @author lihuazeng
 */
public class App {

    public static void main(String[] args) {
        Main.main(args);
    }
}
