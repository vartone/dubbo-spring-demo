package org.example.user;

import org.example.email.api.MailSenderService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[]{"META-INF/spring/application.xml"});
        MailSenderService mailSenderService = (MailSenderService)classPathXmlApplicationContext.getBean("mailSenderService");

        System.out.println("进入注册流程，发送注册邮件。。。");
        boolean r = mailSenderService.send("232@qq.com", "你注册验证码是：168");
        System.out.println("发送注册邮件是否成功："+r);
    }
}
