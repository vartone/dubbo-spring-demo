package org.example.email.api;

/**
 * @author lihuazeng
 */
public interface MailSenderService {
    boolean send(String email, String emailEntry);
}
